/**
 * 
 */
package crawler.blogic;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Cong Tu Thanh Long
 *
 */
public class ProcessWinLost extends HttpRequestHelper {
    
    /**
     * 
     */
    private String _prefixReportFrame;

    /**
     * 
     */
    public ProcessWinLost() {
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     * @param row
     * @param parameter
     * @param url
     * @return
     */
    public String getWinlostContent(String row, List<NameValuePair> parameter, String url) {
        // Define post form
        List<NameValuePair> postParams = new ArrayList<NameValuePair>();

        // Loop in list parameter to update data
        for (NameValuePair namePair : parameter) {
            if (namePair.getName().compareTo("dpFrom") == 0) {
                postParams.add(new BasicNameValuePair(namePair.getName(), row.substring(11, 21)));
                continue;
            }

            if (namePair.getName().compareTo("product") == 0) {
                postParams.add(new BasicNameValuePair(namePair.getName(), row.substring(23, 31)));
                continue;
            }
            postParams.add(namePair);
        }
        
        // Do post data
        String result = null;
        try {
            result = this.sendPost(url, postParams);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // Retur data
        return result;
    }
    
    /**
     * 
     * @param html
     * @return
     * @throws UnsupportedEncodingException
     */
    public List<NameValuePair> getParamWinlostForm(String html) throws UnsupportedEncodingException {
        // Parser document
        Document doc = Jsoup.parse(html);

        // Get form with id
        Element formBetList = doc.getElementsByAttributeValue("id", "form1").first();
        Elements inputElements = formBetList.getElementsByTag("input");
        List<NameValuePair> paramList = new ArrayList<NameValuePair>();

        // Set parameter for action form
        set_prefixReportFrame(formBetList.attr("action"));

        // Parser with key pair
        for (Element inputElement : inputElements) {
            String ekey = inputElement.attr("name");
            String value = inputElement.attr("value");
            
            // Key is mode
            if(ekey.compareTo("mode") == 0) {
                value = "1";
            }

            paramList.add(new BasicNameValuePair(ekey, value));
        }

        // Return list key value
        return paramList;
    }

    /**
     * @return the _prefixReportFrame
     */
    public String get_prefixReportFrame() {
        return _prefixReportFrame;
    }

    /**
     * @param _prefixReportFrame the _prefixReportFrame to set
     */
    public void set_prefixReportFrame(String _prefixReportFrame) {
        this._prefixReportFrame = _prefixReportFrame;
    }

}

package crawler.blogic;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ProcessFrame extends HttpRequestHelper {
    /**
     * 
     */
    private String _frameData;

    /**
     * 
     * @param postParams
     * @param url
     */
    public void getFrameData(List<NameValuePair> postParams, String url) {
        try {
            System.out.println("------------------------------------------------");
            System.out.println("PROCESS FRAME");
            String result = this.sendPost(url, postParams);
            set_frameData(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param postParams
     * @param url
     * @param ids
     */
    public void getFrameData(List<NameValuePair> postParams, String url, String ids) {
        try {
            System.out.println("------------------------------------------------");
            System.out.println("PROCESS FRAME");
            // Update post param for ids
            List<NameValuePair> postForm = new ArrayList<NameValuePair>();

            for (NameValuePair form : postParams) {
                String ekey = form.getName();
                String value = form.getValue();

                // Check key is txId
                if (form.getName().compareTo("ids") == 0) {
                    value = ids;
                }

                postForm.add(new BasicNameValuePair(ekey, value));
            }
            String result = this.sendPost(url, postForm);
            set_frameData(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param s
     * @return
     */
    public List<String> splitString(String s) {
        Pattern pattern = Pattern.compile("f\\(\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(s);

        List<String> output = new ArrayList<String>();

        while (matcher.find()) {
            output.add(matcher.group(1));
        }
        try{
        if (output.size() > 0)
            output.remove(output.size()-1);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return output;
    }

    /**
     * @return the _statementData
     */
    public String get_frameData() {
        return _frameData;
    }

    /**
     * @param _statementData
     *            the _statementData to set
     */
    public void set_frameData(String _frameData) {
        this._frameData = _frameData;
    }
}

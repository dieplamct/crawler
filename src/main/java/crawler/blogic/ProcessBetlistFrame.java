package crawler.blogic;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ProcessBetlistFrame  extends HttpRequestHelper{

    
    public ProcessBetlistFrame()  {
        
    }
    
    /**
     * 
     * @param postParams
     * @param url
     * @param ids
     */
    public String getData(List<NameValuePair> postParams, String url, String ids) {
        String result = null;
        try {
            System.out.println("------------------------------------------------");
            System.out.println("PROCESS BETLIST");
            
            // Update post param for ids
            List<NameValuePair> postForm = new ArrayList<NameValuePair>();
            
            for(NameValuePair form : postParams){
                String ekey = form.getName();
                String value = form.getValue();
                
                // Check key is txId
                if(form.getName().compareTo("ids") == 0){
                    value = ids;
                }
                
                if(form.getName().compareTo("isSplitGamesAndFinancials") == 0){
                    value = "1";
                }

                postForm.add(new BasicNameValuePair(ekey, value));
            }
            result = this.sendPost(url, postForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * 
     * @param s
     * @return
     */
    public List<String> splitString(String s) {
        Pattern pattern = Pattern.compile("f\\(\\[(.*?)\\]\\)");
        Matcher matcher = pattern.matcher(s);

        List<String> output = new ArrayList<String>();

        while (matcher.find()) {
            output.add(matcher.group(1));
        }
        return output;
    }

}

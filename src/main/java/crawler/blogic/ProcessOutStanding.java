package crawler.blogic;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProcessOutStanding extends HttpRequestHelper {
    /**
     * 
     */
    private List<String> _outStandingData;

    /**
     * 
     */
    private String _htmlResult;

    /**
     * 
     */
    private String _prefixForm;
    
    /**
     * 
     */
    public ProcessOutStanding(){
        super();
    }

    /**
     * 
     */
    public void getOutStanding() {
        try {
            setHtmlResult(this.sendGet(
                    "http://"+this._host+"/webroot/restricted/totalbet2/outstanding_new.aspx"));
            //parseStandingResult();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param html
     * @return
     * @throws UnsupportedEncodingException
     */
    public List<NameValuePair> getParamInForm(String html) throws UnsupportedEncodingException {

        // Parser document
        Document doc = Jsoup.parse(html);

        // Get form with id
        Element formBetList = doc.getElementsByAttributeValue("id", "form1").first();
        Elements inputElements = formBetList.getElementsByTag("input");
        List<NameValuePair> paramList = new ArrayList<NameValuePair>();

        // Set parameter for action form
        setPrefixForm(formBetList.attr("action"));

        // Parser with key pair
        for (Element inputElement : inputElements) {
            String ekey = inputElement.attr("name");
            String value = inputElement.attr("value");
            if(ekey.equals("mon")){
                ekey = "product";
            }
            if(ekey.equals("btnRefresh")) continue;
            
            paramList.add(new BasicNameValuePair(ekey, value));
        }

        // Return list key value
        return paramList;
    }

    /**
     * @return the _statementData
     */
    public List<String> getOutStandingData() {
        return _outStandingData;
    }

    /**
     * @param outStandingData
     *            the _statementData to set
     */
    public void setOutStandingData(List<String> outStandingData) {
        this._outStandingData = outStandingData;
    }

    /**
     * @return the _htmlResult
     */
    public String getHtmlResult() {
        return _htmlResult;
    }

    /**
     * @param _htmlResult
     *            the _htmlResult to set
     */
    public void setHtmlResult(String _htmlResult) {
        this._htmlResult = _htmlResult;
    }

    /**
     * @return the _prefixWinlost
     */
    public String getPrefixForm() {
        return _prefixForm;
    }

    /**
     * @param _prefixForm
     *            the _prefixWinlost to set
     */
    public void setPrefixForm(String _prefixForm) {
        this._prefixForm = _prefixForm;
    }
}

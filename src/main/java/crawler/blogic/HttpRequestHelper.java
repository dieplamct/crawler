/**
 * @author Cong Tu Thanh Long
 */
package crawler.blogic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import crawler.constants.CrawlerConstant;

public class HttpRequestHelper extends CrawlerConstant {

    /**
     * 
     */
    private HttpClient client = new DefaultHttpClient();

    /**
     * 
     */
    private Map<String, String> _header;

    /**
     * 
     */
    public HttpRequestHelper() {
        super();
        this.getConfig();
        setHeader(new HashMap<String, String>());
    }

    /**
     * 
     * @param String
     *            url
     * @param List<NameValuePair>
     *            postParams
     */
    public String sendPost(String url, List<NameValuePair> postParams) throws Exception {
        // Init post method
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("Host", this._host);
        post.setHeader("User-Agent", this._userAgent);
        post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.setHeader("Accept-Language", "en-US,en;q=0.5");
        post.setHeader("Cookie", this._cookie);
        post.setHeader("Connection", "keep-alive");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Origin", "http://" + this._host);
        post.setHeader("Upgrade-Insecure-Requests", "1");

        // Set entity
        post.setEntity(new UrlEncodedFormEntity(postParams));

        // Execute request
        HttpResponse response = client.execute(post);

        // Get response status
        int responseCode = response.getStatusLine().getStatusCode();

        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + postParams);
        System.out.println("Post Response Code : " + responseCode);

        // Declare buffer reader
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        // Get result to string
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // Return result
        return result.toString();
    }

    /**
     * 
     * @param String
     *            url
     * @param List<NameValuePair>
     *            postParams
     */
    public String sendPost(String url, List<NameValuePair> postParams, boolean isGetHeader) throws Exception {
        // Init post method
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("Host", this._host);
        post.setHeader("User-Agent", this._userAgent);
        post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.setHeader("Accept-Language", "en-US,en;q=0.5");
        post.setHeader("Cookie", this._cookie);
        post.setHeader("Connection", "keep-alive");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Origin", "http://" + this._host);
        post.setHeader("Upgrade-Insecure-Requests", "1");

        // Set entity
        post.setEntity(new UrlEncodedFormEntity(postParams));

        // Execute request
        HttpResponse response = client.execute(post);

        // Check to get header response
        if (isGetHeader) {
            // Declare map header
            Map<String, String> mpHeader = new HashMap<String, String>();

            // Loop through header
            for (Header header : response.getAllHeaders()) {
                // Check header exist for cookie
                if(mpHeader.containsKey(header.getName())) continue;
                mpHeader.put(header.getName(), header.getValue());
            }

            // Set header member of class
            setHeader(mpHeader);
        }

        // Get response status
        int responseCode = response.getStatusLine().getStatusCode();
        
        // Print debug
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + postParams);
        System.out.println("Post Response Code : " + responseCode);

        // Declare buffer reader
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        // Get result to string
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // Return result
        return result.toString();
    }

    /**
     * 
     * @param String
     *            url request
     * @return String
     */
    public String sendGet(String url) throws Exception {
        // Define get request method
        HttpGet request = new HttpGet(url);

        // Add header
        request.setHeader("User-Agent", this._userAgent);
        request.setHeader("Host", this._host);
        request.setHeader("Cookie", this._cookie);
        request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setHeader("Accept-Language", "en-US,en;q=0.5");
        request.setHeader("Connection", "keep-alive");
        request.setHeader("Upgrade-Insecure-Requests", "1");
        // request.setHeader("Referer", "http://"+ CrawlerConstant.HOST +
        // "/webroot/restricted/HomeLeft.aspx");

        // Execute to get response
        HttpResponse response = client.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();

        // Display monitor debug
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        // Init buffer reader to get result
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        // Get result
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // Return result
        return result.toString();
    }

    /**
     * 
     * @param String
     *            url request
     * @return String
     */
    public String sendGet(String url, boolean isGetHeader) throws Exception {
        // Define get request method
        HttpGet request = new HttpGet(url);

        // Add header
        request.setHeader("User-Agent", this._userAgent);
        request.setHeader("Host", this._host);
        request.setHeader("Cookie", this._cookie);
        request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setHeader("Accept-Language", "en-US,en;q=0.5");
        request.setHeader("Connection", "keep-alive");
        request.setHeader("Upgrade-Insecure-Requests", "1");
        // request.setHeader("Referer", "http://"+ CrawlerConstant.HOST +
        // "/webroot/restricted/HomeLeft.aspx");

        // Execute to get response
        HttpResponse response = client.execute(request);

        // Check to get header response
        if (isGetHeader) {
            // Declare map header
            Map<String, String> mpHeader = new HashMap<String, String>();

            // Loop through header
            for (Header header : response.getAllHeaders()) {
                String value = header.getValue();
                if(mpHeader.containsKey(header.getName())) {
                    value += "; " + mpHeader.get(header.getName());
                }
                mpHeader.put(header.getName(), value);
            }

            // Set header member of class
            setHeader(mpHeader);
        }

        // Get response status
        int responseCode = response.getStatusLine().getStatusCode();

        // Display monitor debug
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        // Init buffer reader to get result
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        // Get result
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // Return result
        return result.toString();
    }

    /**
     * @return the _header
     */
    public Map<String, String> getHeader() {
        return _header;
    }

    /**
     * @param _header
     *            the _header to set
     */
    public void setHeader(Map<String, String> _header) {
        this._header = _header;
    }
}

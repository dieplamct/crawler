/**
 * 
 */
package crawler;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import crawler.blogic.ProcessBetlistFrame;
import crawler.blogic.ProcessFrame;
import crawler.blogic.ProcessOutStanding;
import crawler.constants.CrawlerConstant;
import crawler.db.DB;
import crawler.db.FirstFrameDAO;

/**
 * @author Cong Tu Thanh Long
 *
 */
public class Runner {

    /**
     * 
     */
    static Timer timer;

    /**
     * 
     * @author Cong Tu Thanh Long
     *
     */
    class ProcessData extends TimerTask {
        /**
         * 
         */
        @Override
        public void run() {
            System.out.print("---START PROCESS---");

            // Init db object
            DB db = new DB();

            try {
                // runner.setup();
                // Declare statement
                ProcessOutStanding processOutStanding = new ProcessOutStanding();

                // Declare frame
                ProcessFrame processFrame = new ProcessFrame();

                // Get statement
                processOutStanding.getOutStanding();

                // Prepare form
                List<NameValuePair> statementForm = processOutStanding
                        .getParamInForm(processOutStanding.getHtmlResult());

                // Define winlost url
                String urlFrame = "http://" + processOutStanding.get_host() + "/webroot/restricted/totalbet2/"
                        + processOutStanding.getPrefixForm();

                // Declare url of report frame
                String urlBetList = "http://" + processOutStanding.get_host()
                        + "/webroot/restricted/totalbet2/betlist_frame.aspx";

                // Renew process frame
                processFrame = new ProcessFrame();

                // Defing sql
                String sql, ids;

                // Get frame Data
                processFrame.getFrameData(statementForm, urlFrame);
                List<String> firstFrame = processFrame.splitString(processFrame.get_frameData());

                // Loop throw list result to get sub data
                for (String data : firstFrame) {
                    // Replace string have problem
                    data = data.replaceAll("'", "");

                    // Read data to check exist
                    List<FirstFrameDAO> firstFrameDao = db
                            .readFirstFrame("SELECT * FROM frame WHERE date='" + data.substring(2, 10) + "|" + data.substring(11, 16) + "'");

                    // Check data exit do continue
                    if (firstFrameDao.size() > 0) {
                        FirstFrameDAO frameDao = firstFrameDao.get(0);
                        if(frameDao.getData().equals(data))
                            continue;
                    }

                    // Check result if null do recursive get data, if exist
                    // check
                    sql = "INSERT INTO frame(date, data) VALUES('" + data.substring(2, 10) + "|" + data.substring(11, 16) + "', '" + data + "')";

                    // Execute query
                    db.updateOrInsert(sql);

                    processSecondFrame(db, statementForm, urlFrame, urlBetList, data);
                }
                System.out.print("---SUCCESS---");
            } catch (Exception ex) {
                prepareCookie();
                ex.printStackTrace();
            }
        }

        /**
         * @param db
         * @param statementForm
         * @param urlFrame
         * @param urlBetList
         * @param data
         */
        private void processSecondFrame(DB db, List<NameValuePair> statementForm, String urlFrame, String urlBetList,
                String data) {
            ProcessFrame processFrame;
            String sql;
            String ids;
            // Renew Process frame
            processFrame = new ProcessFrame();
            System.out.println("------------------------------------------------");

            // Get ids
            ids = data.substring(2, 10);

            // Process to get data
            processFrame.getFrameData(statementForm, urlFrame, ids);

            // Parser result data
            List<String> secondFrame = processFrame.splitString(processFrame.get_frameData());

            // Loop list data to recursive get child
            for (String frameEx : secondFrame) {
                frameEx = frameEx.replaceAll("'", "");
                // Get ids
                ids = data.substring(2, 10);

                // Build query prepare insert data
                sql = "INSERT INTO frame_1(ids, data) VALUES('" + ids + "', '" + frameEx + "')";

                // Execute query
                db.updateOrInsert(sql);

                processThirdFrame(db, processFrame, statementForm, urlFrame, urlBetList, data, frameEx);
            }
        }

        /**
         * @param db
         * @param processFrame
         * @param statementForm
         * @param urlFrame
         * @param urlBetList
         * @param data
         * @param frameEx
         */
        private void processThirdFrame(DB db, ProcessFrame processFrame, List<NameValuePair> statementForm,
                String urlFrame, String urlBetList, String data, String frameEx) {
            String sql;
            String ids;
            // Build data
            ids = data.substring(2, 10) + "|" + frameEx.substring(2, 10);

            // Process to get data
            processFrame.getFrameData(statementForm, urlFrame, ids);

            // Parser result data
            List<String> thirdFrame = processFrame.splitString(processFrame.get_frameData());

            // Loop list third frame
            for (String third : thirdFrame) {
                third = third.replaceAll("'", "");
                // Build data
                ids = data.substring(2, 10) + "|" + frameEx.substring(2, 10);

                // Build query prepare insert data
                sql = "INSERT INTO frame_2(ids, data) VALUES('" + ids + "', '" + third + "')";

                // Execute query
                db.updateOrInsert(sql);

                processBetList(db, statementForm, urlBetList, data, frameEx, third);
            }
        }

        /**
         * @param db
         * @param statementForm
         * @param urlBetList
         * @param data
         * @param frameEx
         * @param third
         */
        private void processBetList(DB db, List<NameValuePair> statementForm, String urlBetList, String data,
                String frameEx, String third) {
            String sql;
            String ids;
            // Init betlist object
            ProcessBetlistFrame betList = new ProcessBetlistFrame();

            // Build data
            ids = data.substring(2, 10) + "|" + frameEx.substring(2, 10) + "|" + third.substring(2, 10);

            System.out.println("CALL BET LIST: " + ids);
            String resultBestList = betList.getData(statementForm, urlBetList, ids);
            
            // List resutl betlist
            List<String> lstResultBetList = betList.splitString(resultBestList);
            
            // Show number item of betlist
            System.out.println("CALL BET LIST TOTAL RESULT: " + lstResultBetList.size());
            
            // Declare index to count item of bet list
            int index = 1;

            // Loop list result best list do insert new data
            for (String dataBetList : lstResultBetList) {
                dataBetList = dataBetList.replaceAll("'", "");
                // Insert new data
                sql = "INSERT INTO bet_list(ids, data) VALUES('" + ids + "|" + index + "', '"
                        + dataBetList + "')";
                //sql += " ON DUPLICATE KEY UPDATE data='" + dataBetList.replaceAll("'", "") + "'";

                // Execute query
                db.updateOrInsert(sql);
                
                // In crement index
                index ++;
            }
        }
    }

    /**
     * 
     */
    public Runner() {
        timer = new Timer();
        CrawlerConstant ctn = new CrawlerConstant();
        ctn.getConfig();
        int duration = ctn.get_duration();
        timer.schedule(new ProcessData(), 0, duration * 1000);
    }

    /**
     * 
     */
    private static void prepareCookie() {
        try {
            // Init property
            Properties p = new Properties();
            p.load(new FileInputStream("resource/setting.ini"));
            String username = p.getProperty("user");
            String password = p.getProperty("pass");
            String loginUrl = p.getProperty("loginurl");

            // Define url local web driver
            URL url = new URL("http://127.0.0.1:9515");

            // Init web driver
            final WebDriver driver = new RemoteWebDriver(url, DesiredCapabilities.chrome());
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(loginUrl);

            // Set default data
            driver.findElement(By.name("username")).sendKeys(username);
            driver.findElement(By.name("password")).sendKeys(password);

            // Get button login
            List<WebElement> optionsToSelect = driver.findElements(By.xpath("//input[@class='LoginBtn']"));

            // Do action login
            WebElement option = optionsToSelect.get(0);
            option.click();

            try {
                // Check current url is security
                if (driver.getCurrentUrl().equals(loginUrl + "Security/SecurityCode")) {
                    // Init wait page
                    WebDriverWait wait = new WebDriverWait(driver, 100);

                    // Wait for page load
                    wait.until(new ExpectedCondition<Boolean>() {
                        public Boolean apply(WebDriver wdriver) {
                            return ((RemoteWebDriver) driver).executeScript("return document.readyState")
                                    .equals("complete");
                        }
                    });

                    // Get next page
                    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#LoginBtn")));
                }
            } catch (Exception ex) {

            }

            // CookieStore
            Set<Cookie> cookies = driver.manage().getCookies();

            // Get cookie string
            String cookieString = "";
            StringBuffer sb = new StringBuffer();

            // Loop to get cookie
            for (Cookie cookie : cookies) {
                sb.append(cookie.getName() + "=" + cookie.getValue() + "; ");
            }
            cookieString = sb.toString();
            cookieString = cookieString.substring(0, cookieString.length() - 2);

            // Load property file
            String userAgent = (String) ((RemoteWebDriver) driver).executeScript("return navigator.userAgent", "");
            String currentUrl = driver.getCurrentUrl();
            currentUrl = currentUrl.replaceAll("http://", "");
            currentUrl = currentUrl.replaceAll("/webroot/restricted/home.aspx", "");

            p.setProperty("cookie", cookieString);
            p.setProperty("agent", userAgent);
            p.setProperty("host", currentUrl);
            p.setProperty("duration", "10");
            p.setProperty("user", username);
            p.setProperty("pass", password);
            p.setProperty("loginurl", loginUrl);
            p.store(new FileOutputStream("resource/setting.ini"), "Setting for project running");
        } catch (Exception ex) {
            System.exit(0);
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        prepareCookie();
        // Renew runner
        new Runner();
    }

}

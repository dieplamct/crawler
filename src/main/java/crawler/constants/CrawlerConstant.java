package crawler.constants;

import java.io.FileInputStream;
import java.util.Properties;

public class CrawlerConstant {
    // Define cookie
    public  String _cookie = "";

    // Define user agent
    public  String _userAgent = "";

    // Define host
    public  String _host = "";

    // Duration between process statement
    public  int _duration = 60;

    /**
     * @return the cOOKIE
     */
    public String get_cookie() {
        return _cookie;
    }

    /**
     * @param cookie
     *            the cOOKIE to set
     */
    public  void set_cookie(String cookie) {
        _cookie = cookie;
    }

    /**
     * @return the _userAgent
     */
    public String get_userAgent() {
        return _userAgent;
    }

    /**
     * @param userAgent
     *            the _userAgent to set
     */
    public  void set_userAgent(String userAgent) {
        _userAgent = userAgent;
    }

    /**
     * @return the hOST
     */
    public String get_host() {
        return _host;
    }

    /**
     * @param host
     *            the hOST to set
     */
    public  void set_host(String host) {
        _host = host;
    }

    /**
     * @return the dURATION
     */
    public  int get_duration() {
        return _duration;
    }

    /**
     * @param duration
     *            the dURATION to set
     */
    public  void set_duration(int duration) {
        _duration = duration;
    }

    /**
     * 
     */
    public void getConfig() {
        try {
            Properties p = new Properties();
            p.load(new FileInputStream("resource/setting.ini"));
            System.out.println("cookie: " + p.getProperty("cookie"));
            System.out.println("agent: " + p.getProperty("agent"));
            System.out.println("host: " + p.getProperty("host"));
            System.out.println("duration: " + p.getProperty("duration"));

            set_host(p.getProperty("host"));
            set_userAgent(p.getProperty("agent"));
            set_cookie(p.getProperty("cookie"));
            set_duration(Integer.parseInt(p.getProperty("duration")));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

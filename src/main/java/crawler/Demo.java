package crawler;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import crawler.authentication.FormAuthInfo;
import crawler.blogic.HttpRequestHelper;
import crawler.constants.CrawlerConstant;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Demo {

    public static void main(String args[]) {
        String username = "aaa666666";
        String password = "haikim6789.";
        String loginUrl = "https://agent.sbobet.com/";

        try {
            String urlHome = "http://45acjjn8gaxf.sbobet.com/webroot/restricted/home.aspx";
            urlHome = urlHome.replaceAll("http://", "");
            urlHome = urlHome.replaceAll("/webroot/restricted/home.aspx", "");
            
            URL url = new URL("http://127.0.0.1:9515");
            
            final WebDriver driver = new RemoteWebDriver(url, DesiredCapabilities.chrome()) ;
            
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(loginUrl);
            
            String userAgent = (String) ((RemoteWebDriver) driver).executeScript("return navigator.userAgent","");
            System.out.println(userAgent);
            // Set default data
            driver.findElement(By.name("username")).sendKeys(username);
            driver.findElement(By.name("password")).sendKeys(password);
            
            // Get button login
            List<WebElement> optionsToSelect = driver.findElements(By.xpath("//input[@class='LoginBtn']"));

            // Do action login
            WebElement option = optionsToSelect.get(0);
            option.click();
            try{
             // Init wait page
                WebDriverWait wait = new WebDriverWait(driver, 1000);

                // Wait for page load
                wait.until(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver wdriver) {
                        return ((RemoteWebDriver) driver).executeScript(
                            "return document.readyState"
                        ).equals("complete");
                    }
                });
                
                // Get next page
                wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#LoginBtn")));
            }
            catch(Exception ex){
                
            }
            
            // CookieStore
            Set<Cookie> cookies = driver.manage().getCookies();
            
            // Get cookie string
            StringBuffer sb = new StringBuffer();
            
            for (Cookie cookie: cookies) {
              System.out.println("CookieHandler retrieved cookie: " + cookie.getName() + " = " + cookie.getValue());
              sb.append(cookie.getName() + "=" + cookie.getValue() + "; ");
            }
            String cookieString = sb.toString();
            cookieString = cookieString.substring(0, cookieString.length() -2);
            System.out.println(cookieString);
            Properties p = new Properties();
            p.setProperty("cookie", cookieString);
            p.setProperty("agent", userAgent);
            p.setProperty("host", "45acjjn8gaxf.com3456.com");
            p.setProperty("duration", "10");
            p.store(new FileOutputStream("resource/setting.ini"), "Setting for project running");
            
            // Declare variable
            String cookie = "";
            boolean isGetHeader = true;
            Map<String, String> header = null;
            String token = "";
            
         // Define http request
            HttpRequestHelper http = new HttpRequestHelper();
            
            http.set_host("agent.sbobet.com");
            http.set_userAgent(
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");

            // Get html form login
            String html = http.sendGet(loginUrl, isGetHeader);
            header = http.getHeader();

            driver.get(loginUrl);
            // Parser document
            Document doc = Jsoup.parse(html);

            // Get form with id
            Element formBetList = doc.getElementsByAttributeValue("id", "loginForm").first();
            Elements inputElements = formBetList.getElementsByTag("input");
            List<NameValuePair> paramList = new ArrayList<NameValuePair>();

            // Get action
            String action = formBetList.attr("action");

            // Parser with key pair
            for (Element inputElement : inputElements) {
                String ekey = inputElement.attr("name");
                String value = inputElement.attr("value");

                if (ekey.equals("username")) {
                    value = username;
                }
                if (ekey.equals("password")) {
                    value = password;
                }
                if (ekey.equals("__RequestVerificationToken")) {
                    token = value;
                }
                paramList.add(new BasicNameValuePair(ekey, value));
            }

            System.out.println("key: " + action);
            String urlPost = "https://" + http.get_host() + action;
            http.set_cookie(header.get("Set-Cookie"));
            html = http.sendPost(urlPost, paramList, isGetHeader);
            header = http.getHeader();
            cookie = http.get_cookie().replaceFirst("ASP.NET_SessionId=", header.get("Set-Cookie").substring(0, 42) + "; lang=EN");
            cookie = cookie.replaceFirst("path=/; HttpOnly;Secure; lang=EN; path=/;Secure; ", "__utmt=1; __utma=71703244.1150871559.1471926453.1471926453.1471926453.1; __utmb=71703244.1.10.1471926453; __utmc=71703244; __utmz=71703244.1471926453.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ");
            cookie = cookie.replaceFirst("HttpOnly;Secure", "");
            cookie = cookie.replaceFirst("path=/;", "");
            http.set_cookie(cookie);
            
            // Continue get resource
            String urlTransfer = "https://" + http.get_host() + header.get("Location");

            // Continue get Login
            html = http.sendGet(urlTransfer, isGetHeader);
            header = http.getHeader();
            System.out.println("Location: " + header.get("Location"));
            
            // Get url Security
            urlTransfer = "https://" + http.get_host() + header.get("Location");

            // Continue get Security
            html = http.sendGet(urlTransfer, isGetHeader);
            header = http.getHeader();
            System.out.println(html);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
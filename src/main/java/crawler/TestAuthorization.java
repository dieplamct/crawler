package crawler;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.CookiePolicy;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

//import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.box.boxjavalibv2.BoxClient;
import com.box.boxjavalibv2.dao.BoxOAuthToken;
import com.box.boxjavalibv2.exceptions.AuthFatalFailureException;
import com.box.boxjavalibv2.exceptions.BoxServerException;
import com.box.boxjavalibv2.requests.requestobjects.BoxOAuthRequestObject;
import com.box.restclientv2.exceptions.BoxRestException;

public class TestAuthorization {

    public static final int PORT = 4000;
    public static final String key = "JdHkklWTdfTNwJtiDOoYleJOADVIOmRd";
    public static final String secret = "CVQwZxOkTQEqYgf4FMccXXVd1zpn5lT2";

    private final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    private List<String> cookies;
    private HttpClient client = new DefaultHttpClient();
    /*
        Request URL:https://agent.sbobet.com/
        Request Method:GET
        Status Code:200 OK
        Remote Address:103.11.43.52:443
        
        Response Headers
        view source
        Cache-Control:no-cache, no-store
        Content-Encoding:gzip
        Content-Length:2081
        Content-Type:text/html; charset=utf-8
        Date:Tue, 23 Aug 2016 07:44:14 GMT
        Expires:-1
        Pragma:no-cache
        Set-Cookie:lang=EN; path=/;Secure
        Set-Cookie:__RequestVerificationToken=2mYrk982rW26uPvTkEgh2zikUR4B4ewp-E4uuhmR5ob0VLyAvxuqNdIsVB3afUQTvRB725VCZUBK3iYU3r53qGGzl9N-_JzVrADa4Fz388s1; path=/; HttpOnly;Secure
        Strict-Transport-Security:max-age=63072000
        Vary:Accept-Encoding
        X-Frame-Options:SAMEORIGIN
        
        Request Headers
        view source
        Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/*;q=0.8
        Accept-Encoding:gzip, deflate, sdch, br
        Accept-Language:en-US,en;q=0.8,vi;q=0.6
        Cache-Control:max-age=0
        Connection:keep-alive
        Host:agent.sbobet.com
        Upgrade-Insecure-Requests:1
        User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        */
    /*
        Request URL:https://agent.sbobet.com/Captcha
        Request Method:POST
        Status Code:302 Found
        Remote Address:103.11.43.52:443
        
        Response Headers
        view source
        Cache-Control:no-cache, no-store
        Content-Length:129
        Content-Type:text/html; charset=utf-8
        Date:Tue, 23 Aug 2016 07:54:12 GMT
        Expires:-1
        Location:/Login/Login
        Pragma:no-cache
        Set-Cookie:ASP.NET_SessionId=lxjqcrex4szm2wt2vruh2frz; path=/; HttpOnly;Secure
        Set-Cookie:lang=EN; path=/;Secure
        Strict-Transport-Security:max-age=63072000
        X-Frame-Options:SAMEORIGIN
        
        Request Headers
        view source
        Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/*;q=0.8
        Accept-Encoding:gzip, deflate, br
        Accept-Language:en-US,en;q=0.8,vi;q=0.6
        Cache-Control:max-age=0
        Connection:keep-alive
        Content-Length:201
        Content-Type:application/x-www-form-urlencoded
        Cookie:lang=EN; __RequestVerificationToken=2mYrk982rW26uPvTkEgh2zikUR4B4ewp-E4uuhmR5ob0VLyAvxuqNdIsVB3afUQTvRB725VCZUBK3iYU3r53qGGzl9N-_JzVrADa4Fz388s1; __utmt=1; __utma=71703244.1060090471.1471938255.1471938255.1471938255.1; __utmb=71703244.1.10.1471938255; __utmc=71703244; __utmz=71703244.1471938255.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)
        Host:agent.sbobet.com
        Origin:https://agent.sbobet.com
        Referer:https://agent.sbobet.com/
        Upgrade-Insecure-Requests:1
        User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        
        Form Data
        view source
        view URL encoded
        __RequestVerificationToken:bDDiwnOL4jm5xNWx0RuAya3LoimbYKojtWcCWXY-aw7GQopUgSGtCrNcjIDM59XnaARPdgUQxlmzwg6pi_kOT6CS_iMSv2B453EZs_DUgIw1
        username:aaa666666
        password:haikim6789.
        lang:EN
        btnSubmit:Sign In
        */
    /*
        Request URL:https://agent.sbobet.com/Login/Login
        Request Method:GET
        Status Code:302 Found
        Remote Address:103.11.43.52:443
        Response Headers
        view source
        Cache-Control:no-cache, no-store
        Content-Length:126
        Content-Type:text/html; charset=utf-8
        Date:Tue, 23 Aug 2016 07:54:12 GMT
        Expires:-1
        Location:/Security
        Pragma:no-cache
        Set-Cookie:lang=EN; path=/;Secure
        Strict-Transport-Security:max-age=63072000
        X-Frame-Options:SAMEORIGIN
        Request Headers
        view source
        Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/*;q=0.8
        Accept-Encoding:gzip, deflate, sdch, br
        Accept-Language:en-US,en;q=0.8,vi;q=0.6
        Cache-Control:max-age=0
        Connection:keep-alive
        Cookie:__RequestVerificationToken=2mYrk982rW26uPvTkEgh2zikUR4B4ewp-E4uuhmR5ob0VLyAvxuqNdIsVB3afUQTvRB725VCZUBK3iYU3r53qGGzl9N-_JzVrADa4Fz388s1; __utmt=1; __utma=71703244.1060090471.1471938255.1471938255.1471938255.1; __utmb=71703244.1.10.1471938255; __utmc=71703244; __utmz=71703244.1471938255.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ASP.NET_SessionId=lxjqcrex4szm2wt2vruh2frz; lang=EN
        Host:agent.sbobet.com
        Referer:https://agent.sbobet.com/
        Upgrade-Insecure-Requests:1
        User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
    */
    
    /*
        Request URL:https://agent.sbobet.com/Security
        Request Method:GET
        Status Code:302 Found
        Remote Address:103.11.43.52:443
        Response Headers
        view source
        Cache-Control:no-cache, no-store
        Content-Length:139
        Content-Type:text/html; charset=utf-8
        Date:Tue, 23 Aug 2016 07:54:12 GMT
        Expires:-1
        Location:/Security/SecurityCode
        Pragma:no-cache
        Strict-Transport-Security:max-age=63072000
        X-Frame-Options:SAMEORIGIN
        Request Headers
        view source
        Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/*;q=0.8
        Accept-Encoding:gzip, deflate, sdch, br
        Accept-Language:en-US,en;q=0.8,vi;q=0.6
        Cache-Control:max-age=0
        Connection:keep-alive
        Cookie:__RequestVerificationToken=2mYrk982rW26uPvTkEgh2zikUR4B4ewp-E4uuhmR5ob0VLyAvxuqNdIsVB3afUQTvRB725VCZUBK3iYU3r53qGGzl9N-_JzVrADa4Fz388s1; __utmt=1; __utma=71703244.1060090471.1471938255.1471938255.1471938255.1; __utmb=71703244.1.10.1471938255; __utmc=71703244; __utmz=71703244.1471938255.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ASP.NET_SessionId=lxjqcrex4szm2wt2vruh2frz; lang=EN
        Host:agent.sbobet.com
        Referer:https://agent.sbobet.com/
        Upgrade-Insecure-Requests:1
        User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        */
    
    /*
        Request URL:https://agent.sbobet.com/Security/SecurityCode
        Request Method:GET
        Status Code:200 OK
        Remote Address:103.11.43.52:443
        Response Headers
        view source
        Cache-Control:no-cache, no-store
        Content-Encoding:gzip
        Content-Length:1877
        Content-Type:text/html; charset=utf-8
        Date:Tue, 23 Aug 2016 07:54:12 GMT
        Expires:-1
        Pragma:no-cache
        Strict-Transport-Security:max-age=63072000
        Vary:Accept-Encoding
        X-Frame-Options:SAMEORIGIN
        Request Headers
        view source
        Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/*;q=0.8
        Accept-Encoding:gzip, deflate, sdch, br
        Accept-Language:en-US,en;q=0.8,vi;q=0.6
        Cache-Control:max-age=0
        Connection:keep-alive
        Cookie:__RequestVerificationToken=2mYrk982rW26uPvTkEgh2zikUR4B4ewp-E4uuhmR5ob0VLyAvxuqNdIsVB3afUQTvRB725VCZUBK3iYU3r53qGGzl9N-_JzVrADa4Fz388s1; __utmt=1; __utma=71703244.1060090471.1471938255.1471938255.1471938255.1; __utmb=71703244.1.10.1471938255; __utmc=71703244; __utmz=71703244.1471938255.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ASP.NET_SessionId=lxjqcrex4szm2wt2vruh2frz; lang=EN
        Host:agent.sbobet.com
        Referer:https://agent.sbobet.com/
        Upgrade-Insecure-Requests:1
        User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
     */
        
    /**
     * @param args the command line arguments
     */

    private static final String SEARCH_TERM = "Tab";
    private static final String ADDRESS = "https://agent.sbobet.com/";
    private static final CookieManager COOKIEMANAGER = new CookieManager();
    private static Object myCookie;
    
    private static Object checkForCookie(Object[] cookieJar)
    {
        for (int i = 0; i < cookieJar.length; ++i)
        {
            if (cookieJar[i].equals(SEARCH_TERM))
            {
                return cookieJar[i];
            }
        }
        return "Cookie not found";
    }
    
//    CookieHandler retrieved cookie: __utmz = 71703244.1472012524.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)
//    CookieHandler retrieved cookie: ASP.NET_SessionId = xnsgwk2waekx4gxass4qkvb5
//    CookieHandler retrieved cookie: __RequestVerificationToken = ApxYu2ZQc4QNQKoNKX1lX2BjF4Gu8R2pYXwRzS8EV4pGo0uq5l1Ayk7CXnvWSukt8fxT6pZWjIvrHNtX0uCeieO5H8gpSkFAqgp63Nez-S01
//    CookieHandler retrieved cookie: __utmt = 1
//    CookieHandler retrieved cookie: __utmb = 71703244.2.10.1472012524
//    CookieHandler retrieved cookie: __utmc = 71703244
//    CookieHandler retrieved cookie: lang = EN
//    CookieHandler retrieved cookie: __utma = 71703244.492771931.1472012524.1472012524.1472012524.1
//    __utmz=71703244.1472012524.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); expires=Wed, 22 Feb 2017 11:22:05 ICT; path=/; domain=.sbobet.comASP.NET_SessionId=xnsgwk2waekx4gxass4qkvb5; path=/; domain=agent.sbobet.com;secure;__RequestVerificationToken=ApxYu2ZQc4QNQKoNKX1lX2BjF4Gu8R2pYXwRzS8EV4pGo0uq5l1Ayk7CXnvWSukt8fxT6pZWjIvrHNtX0uCeieO5H8gpSkFAqgp63Nez-S01; path=/; domain=agent.sbobet.com;secure;__utmt=1; expires=Wed, 24 Aug 2016 11:32:04 ICT; path=/; domain=.sbobet.com__utmb=71703244.2.10.1472012524; expires=Wed, 24 Aug 2016 11:52:05 ICT; path=/; domain=.sbobet.com__utmc=71703244; path=/; domain=.sbobet.comlang=EN; path=/; domain=agent.sbobet.com;secure;__utma=71703244.492771931.1472012524.1472012524.1472012524.1; expires=Fri, 24 Aug 2018 11:22:05 ICT; path=/; domain=.sbobet.com

    public static void main(String[] args)
            throws AuthFatalFailureException, BoxServerException, BoxRestException, Exception {

     // Instantiate CookieManager;
        // make sure to set CookiePolicy
        CookieManager manager = new CookieManager();
        manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);

        // get content from URLConnection;
        // cookies are set by web site
        URL urlObj = new URL("https://agent.sbobet.com/");
        URLConnection connection = urlObj.openConnection();
        connection.getContent();

        // get cookies from underlying
        // CookieStore
        CookieStore cookieJar =  manager.getCookieStore();
        List <HttpCookie> cookies =
            cookieJar.getCookies();
        for (HttpCookie cookie: cookies) {
          System.out.println("CookieHandler retrieved cookie: " + cookie);
        }
        
        if (key.startsWith("YOUR")) {
            System.out.println("Before this sample app will work, you will need to change the");
            System.out.println("'key' and 'secret' values in the source code.");
            return;
        }

        File input = new File("/Users/baodiep/Documents/workspace/play-arround-curl/src/crawler/NewFile.html");
        Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");

        Elements inputElements = doc.select("#form1 table tr tr td");

        boolean flagDoAct = false;
        List<String> statementData = new ArrayList<String>();

        for (Element element : inputElements) {
            if (flagDoAct == false && element.text().compareTo("Total Balance") == 0) {
                flagDoAct = true;
                continue;
            }

            Elements childElement = element.getElementsByTag("a");
            if (flagDoAct && childElement.hasAttr("onclick")) {
                System.out.println("VALUE: " + element.text());
                statementData.add(childElement.attr("onclick"));
            }
        }

        // 1. Get
        // http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/statement_new.aspx?P=Statement
        // 2. Post
        // http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/report_frame.aspx

        String code = "";
        // String url =
        // "https://app.box.com/api/oauth2/authorize?response_type=code&client_id="
        // + key + "&redirect_uri=http%3A//localhost%3A" + PORT;
        String url = "http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/winlost.aspx?ek=5";
        // http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/report_frame.aspx
        // p:32
        // ek:9
        // mode:1
        // chart:
        // ids:
        // isSplitGamesAndFinancials:
        // product:1
        // dpFrom:08/10/2016
        // dpTo:08/10/2016
        //[p=32, ek=7, mode=0, chart=, ids=, isSplitGamesAndFinancials=, btnViewReport=View History Report, dpFrom=08/10/2016, dpTo=08/10/2016, btnSettingSubmit=Submit]

        // TestAuthorization http = new TestAuthorization();

        // make sure cookies is turn on
        // CookieHandler.setDefault(new CookieManager());

        // String page = http.GetPageContent(url);
        //
        // Map<String,String> loginFormData = new HashMap<String, String>();
        // loginFormData.put("username", "dieplb");
        // loginFormData.put("password", "HoaSua@1325");
        // List<NameValuePair> postParams = http.getParams(page,
        // "fm1",loginFormData);

        // http.sendPost(url, postParams,false);
        // String result = http
        // .sendGet("http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/statement_new.aspx?P=Statement");
        // System.out.println(result);
        // Map<String,String> grantFormData = new HashMap<String, String>();
        // grantFormData.put("consent_reject", "");
        // List<NameValuePair> grantParams =
        // http.getParams(grantpage,"consent_form",grantFormData);
        // code = http.sendPost(url,grantParams,true);
        // System.out.println(code);
        //
        // BoxClient client = getAuthenticatedClient(code);
        //
        // BoxFolder boxFolder= client.getFoldersManager().getFolder("0",null);
        // ArrayList<BoxTypedObject> folderEntries =
        // boxFolder.getItemCollection().getEntries();
        // int folderSize = folderEntries.size();
        // for (int i = 0; i <= folderSize-1; i++){
        // BoxTypedObject folderEntry = folderEntries.get(i);
        // String name = (folderEntry instanceof BoxItem) ?
        // ((BoxItem)folderEntry).getName() : "(unknown)";
        // System.out.println("i:" + i + ", Type:" + folderEntry.getType() + ",
        // Id:" + folderEntry.getId() + ", Name:" + name);
        // }
        //
        // BoxEventsManager boxEventsManager = client.getEventsManager();
        // BoxEventCollection eventsCollection =
        // boxEventsManager.getEvents(BoxEventRequestObject.getEventsRequestObject(0));
        // for (BoxTypedObject item : eventsCollection.getEntries()) {
        // BoxEvent event = (BoxEvent) item;
        // System.out.println("[Events]Id:" + event.getId() + ", Type:" +
        // event.getEventType());
        // }
        // "http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/winlost.aspx?ek=5";
        // isParent:1
        // txId:-1
        // product:1
        // dpFrom:08/03/2016
        // mode:0
        // File input = new File("/tmp/input.html");
        // Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");

        // p:32
        // ek:8
        // mode:1
        // chart:
        // ids:
        // isSplitGamesAndFinancials:
        // product:1
        // dpFrom:08/07/2016
        // dpTo:08/07/2016
        ////[p=32, ek=7, mode=0, chart=, ids=, isSplitGamesAndFinancials=, btnViewReport=View History Report, dpFrom=08/10/2016, dpTo=08/10/2016, btnSettingSubmit=Submit]

        // =================

        // p:32
        // ek:8
        // mode:1
        // chart:
        // ids:12324824
        // isSplitGamesAndFinancials:
        // product:1
        // dpFrom:08/07/2016
        // dpTo:08/07/2016

        // =========================

        // p:32
        // ek:8
        // mode:1
        // chart:
        // ids:12324824|12325035
        // isSplitGamesAndFinancials:
        // product:1
        // dpFrom:08/07/2016
        // dpTo:08/07/2016

        // ==================
        // http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/betlist_frame.aspx?prod=1
        // p:32
        // ek:8
        // mode:1
        // chart:
        // ids:12324824|12325035|14473379
        // isSplitGamesAndFinancials:1
        // product:1
        // dpFrom:08/07/2016
        // dpTo:08/07/2016
    }

    private static BoxClient getAuthenticatedClient(String code)
            throws BoxRestException, BoxServerException, AuthFatalFailureException {
        BoxClient client = new BoxClient(key, secret);
        BoxOAuthRequestObject obj = BoxOAuthRequestObject.createOAuthRequestObject(code, key, secret,
                "http://localhost:" + PORT);
        BoxOAuthToken bt = client.getOAuthManager().createOAuth(obj);
        client.authenticate(bt);
        return client;
    }

    private String sendPost(String url, List<NameValuePair> postParams, boolean getAuthCode) throws Exception {

        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("Host", "clc.fsoft.com.vn");
        post.setHeader("User-Agent", USER_AGENT);
        post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.setHeader("Accept-Language", "en-US,en;q=0.5");
        List<String> lstCookie = getCookies();
        post.setHeader("Cookie", lstCookie.toString().substring(1, lstCookie.toString().length() - 1));
        post.setHeader("Connection", "keep-alive");
        post.setHeader("Referer",
                "http://45acjjn8gaxf.com3456.com/webroot/restricted/report2/statement_new.aspx?P=Statement");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Host", "45acjjn8gaxf.com3456.com");
        post.setHeader("Origin", "http://45acjjn8gaxf.com3456.com");

        post.setEntity(new UrlEncodedFormEntity(postParams));

        HttpResponse response = client.execute(post);

        int responseCode = response.getStatusLine().getStatusCode();

        // System.out.println("\nSending 'POST' request to URL : " + url);
        // System.out.println("Post parameters : " + postParams);
        System.out.println("Response Code : " + responseCode);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        lstCookie = new ArrayList<String>();

        // get response headers
        String code = "";
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            // System.out.println("[Response Header] Name: " + header.getName()
            // + " Value: " + header.getValue());
            if (header.getName().equals("Location")) {
                code = header.getValue().substring(header.getValue().indexOf("code=") + 5);
            }

            if (header.getName().equals("Set-Cookie")) {
                // code =
                // header.getValue().substring(header.getValue().indexOf("code=")+5);
                lstCookie.add(header.getValue());
            }
        }

        // if(lstCookie.size() > 0){
        // setCookies(lstCookie);
        // }

        if (getAuthCode)
            return code;
        else
            return result.toString();
    }

    private String sendGet(String url) throws Exception {
        HttpGet request = new HttpGet(url);

        request.setHeader("User-Agent", USER_AGENT);
        // request.setHeader("Host", "clc.fsoft.com.vn");
        request.setHeader("Host", "45acjjn8gaxf.com3456.com");
        List<String> lstCookie = getCookies();
        // request.setHeader("Cookie", lstCookie.toString().substring(1,
        // lstCookie.toString().length() - 1));
        request.setHeader("Cookie",
                "_ga=GA1.2.1046941174.1470750894; _ga=GA1.3.1046941174.1470750894; AWSUSER_ID=awsuser_id1470752289568r2369; ASP.NET_SessionId=e131qydd2vgnuurcf5vfkjsy");
        request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setHeader("Accept-Language", "en-US,en;q=0.5");
        request.setHeader("Connection", "keep-alive");
        // request.setHeader("Upgrade-Insecure-Requests", "1");
        // request.setHeader("Referer",
        // "https://clc.fsoft.com.vn/cas/login?service=https%3A%2F%2Fclc.fsoft.com.vn%2Fsakai-login-tool%2Fcontainer");

        HttpResponse response = client.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();

        // System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // set cookies
        lstCookie = new ArrayList<String>();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals("Set-Cookie")) {
                // code =
                // header.getValue().substring(header.getValue().indexOf("code=")+5);
                lstCookie.add(header.getValue());
            }
        }

        if (lstCookie.size() > 0) {
            setCookies(lstCookie);
        }

        return result.toString();
    }

    private String GetPageContent(String url) throws Exception {

        HttpGet request = new HttpGet(url);

        request.setHeader("User-Agent", USER_AGENT);
        request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        request.setHeader("Accept-Language", "en-US,en;q=0.5");

        HttpResponse response = client.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();

        // System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // set cookies
        // setCookies(response.getFirstHeader("Set-Cookie") == null ? ""
        // : response.getFirstHeader("Set-Cookie").toString());
        List<String> lstCookie = new ArrayList<String>();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals("Set-Cookie")) {
                // code =
                // header.getValue().substring(header.getValue().indexOf("code=")+5);
                lstCookie.add(header.getValue());
            }
        }

        if (lstCookie.size() > 0) {
            setCookies(lstCookie);
        }

        return result.toString();

    }

    public List<NameValuePair> getParams(String html, String formId, Map<String, String> formdata)
            throws UnsupportedEncodingException {

        System.out.println("Extracting form's data...");

        Document doc = Jsoup.parse(html);

        Element loginform = doc.getElementsByAttributeValue("id", formId).first();
        Elements inputElements = loginform.getElementsByTag("input");
        List<NameValuePair> paramList = new ArrayList<NameValuePair>();

        for (Element inputElement : inputElements) {
            String ekey = inputElement.attr("name");
            String value = inputElement.attr("value");

            for (String datakey : formdata.keySet()) {
                if (ekey.equals(datakey))
                    value = formdata.get(datakey);
            }
            paramList.add(new BasicNameValuePair(ekey, value));
        }
        return paramList;
    }

    public List<String> getCookies() {
        return cookies;
    }

    public void setCookies(List<String> cookies) {
        this.cookies = cookies;
    }
}

/**
 * 
 */
package crawler.db;

import java.util.ArrayList;
import java.util.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
/**
 * @author Cong Tu Thanh Long
 *
 */
// STEP 1. Import required packages
import java.sql.*;
import java.util.Properties;

public class DB {
    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private String _dbUrl;
    private String _dbName;
    private String _dbPort;

    // Database credentials
    private String _user;
    private String _pass;

    public DB() {
        Properties p = new Properties();
        try {
            p.load(new FileInputStream("resource/jdbc.ini"));
            System.out.println("DBhost: " + p.getProperty("host"));
            System.out.println("DBport: " + p.getProperty("port"));
            System.out.println("DBName: " + p.getProperty("db"));
            System.out.println("DBuser: " + p.getProperty("user"));
            System.out.println("DBpass: " + p.getProperty("pass"));
            set_dbUrl(p.getProperty("host"));
            set_dbName(p.getProperty("db"));
            set_dbPort(p.getProperty("port"));
            set_user(p.getProperty("user"));
            set_pass(p.getProperty("pass"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param sql
     * @return
     */
    public List<FirstFrameDAO> readFirstFrame(String sql) {
        Connection conn = null;
        Statement stmt = null;
        List<FirstFrameDAO> lstResult = new ArrayList<FirstFrameDAO>();
        try {
            // STEP 2: Register JDBC driver
            Class.forName(DB.JDBC_DRIVER);

            // STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection("jdbc:mysql://"+get_dbUrl()+":"+get_dbPort()+"/" + get_dbName(), get_user(), get_pass());

            // STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int id = rs.getInt("id");
                String date = rs.getString("date");
                String data = rs.getString("data");
                Date createDate = rs.getDate("create_date");
                Date updateDate = rs.getDate("update_date");
                
                FirstFrameDAO dd = new FirstFrameDAO();
                dd.setId(id);
                dd.setDate(date);
                dd.setData(data);
                dd.setCreateDate(createDate);
                dd.setUpdateDate(updateDate);
                
                lstResult.add(dd);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        System.out.println("Goodbye!");
        return lstResult;
    }
    
    /**
     * 
     * @param sql
     * @return
     */
    public List<FrameDAO> readFrame(String sql) {
        Connection conn = null;
        Statement stmt = null;
        List<FrameDAO> lstResult = new ArrayList<FrameDAO>();
        try {
            // STEP 2: Register JDBC driver
            Class.forName(DB.JDBC_DRIVER);

            // STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection("jdbc:mysql://"+get_dbUrl()+":"+get_dbPort()+"/" + get_dbName(), get_user(), get_pass());

            // STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                int id = rs.getInt("id");
                String ids = rs.getString("ids");
                String data = rs.getString("data");
                Date createDate = rs.getDate("create_date");
                Date updateDate = rs.getDate("update_date");
                
                FrameDAO dd = new FrameDAO();
                dd.setId(id);
                dd.setIds(ids);
                dd.setData(data);
                dd.setCreateDate(createDate);
                dd.setUpdateDate(updateDate);
                
                lstResult.add(dd);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        System.out.println("Goodbye!");
        return lstResult;
    }
    
    /**
     * 
     * @param sql
     * @return
     */
    public List<BetListDAO> readBetList(String sql) {
        Connection conn = null;
        Statement stmt = null;
        List<BetListDAO> lstResult = new ArrayList<BetListDAO>();
        try {
            // STEP 2: Register JDBC driver
            Class.forName(DB.JDBC_DRIVER);

            // STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection("jdbc:mysql://"+get_dbUrl()+":"+get_dbPort()+"/" + get_dbName(), get_user(), get_pass());

            // STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            // STEP 5: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                String ids = rs.getString("ids");
                String data = rs.getString("data");
                Date createDate = rs.getDate("create_date");
                Date updateDate = rs.getDate("update_date");
                
                BetListDAO dd = new BetListDAO();
                dd.setIds(ids);
                dd.setData(data);
                dd.setCreateDate(createDate);
                dd.setUpdateDate(updateDate);
                
                lstResult.add(dd);
            }
            // STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        System.out.println("Goodbye!");
        return lstResult;
    }
    
    /**
     * 
     * @param sql
     * @return
     */
    public int updateOrInsert(String sql) {
        Connection conn = null;
        Statement stmt = null;
        int result = 0;
        try {
            // STEP 2: Register JDBC driver
            Class.forName(DB.JDBC_DRIVER);

            // STEP 3: Open a connection
            conn = DriverManager.getConnection("jdbc:mysql://"+get_dbUrl()+":"+get_dbPort()+"/" + get_dbName(), get_user(), get_pass());

            // STEP 4: Execute a query
            stmt = conn.createStatement();
            result = stmt.executeUpdate(sql);
            
            // STEP 6: Clean-up environment
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        return result;
    }

    /**
     * @return the dB_URL
     */
    public String get_dbUrl() {
        return _dbUrl;
    }

    /**
     * @param dB_URL the dB_URL to set
     */
    public void set_dbUrl(String dB_URL) {
        _dbUrl = dB_URL;
    }

    /**
     * @return the dB_NAME
     */
    public String get_dbName() {
        return _dbName;
    }

    /**
     * @param dB_NAME the dB_NAME to set
     */
    public void set_dbName(String dB_NAME) {
        _dbName = dB_NAME;
    }

    /**
     * @return the uSER
     */
    public String get_user() {
        return _user;
    }

    /**
     * @param uSER the uSER to set
     */
    public void set_user(String uSER) {
        _user = uSER;
    }

    /**
     * @return the pASS
     */
    public String get_pass() {
        return _pass;
    }

    /**
     * @param pASS the pASS to set
     */
    public void set_pass(String pASS) {
        _pass = pASS;
    }

    /**
     * @return the _dbPort
     */
    public String get_dbPort() {
        return _dbPort;
    }

    /**
     * @param _dbPort the _dbPort to set
     */
    public void set_dbPort(String _dbPort) {
        this._dbPort = _dbPort;
    }
}
